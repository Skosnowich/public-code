# Generics

`io.gitlab.skosnowich.publiccode.generics`

You will write a class `Controller` which calls methods of other classes and returns their results.
The `Controller` class has one method with single parameter of type `RequestType` (best should be an enum).
The `RequestType` can have two values: `A` and `B`.
Dependant on the `RequestType` it calls either service class `ServiceA` or `ServiceB`.

The services each have a single public method which will return a response-class (`ResponseA` and `ResponseB`) AND a boolean flag as a status.
`ResponseA` will have a single method `specificMethodForA` and `ResponseB` will have a single method `specificMethodForB`.

The `Controller` will, after getting the result from the service, call either `specificMethodForA` or `specificMethodForB` and then return the result.
