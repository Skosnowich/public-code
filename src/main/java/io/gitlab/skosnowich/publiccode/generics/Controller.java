package io.gitlab.skosnowich.publiccode.generics;

class Controller {

	private final ServiceA serviceA;
	private final ServiceB serviceB;

	Controller(ServiceA serviceA, ServiceB serviceB) {
		this.serviceA = serviceA;
		this.serviceB = serviceB;
	}

	void callService(RequestType requestType) {
		if (requestType == RequestType.A) {
			callServiceA();
		} else if (requestType == RequestType.B) {
			callServiceB();
		}
	}

	private void callServiceB() {
		Result<ResponseB> result = serviceB.serviceMethod();
		if (result.isSuccess()) {
			result.getResponse().specificMethodForB();
		}
	}

	private void callServiceA() {
		Result<ResponseA> result = serviceA.serviceMethod();
		if (result.isSuccess()) {
			result.getResponse().specificMethodForA();
		}
	}

}
