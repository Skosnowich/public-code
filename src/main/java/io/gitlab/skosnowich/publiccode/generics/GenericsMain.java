package io.gitlab.skosnowich.publiccode.generics;

public class GenericsMain {

	public static void main(String[] args) {
		Controller controller = new Controller(new ServiceA(), new ServiceB());

		controller.callService(RequestType.A);
		controller.callService(RequestType.B);
	}

}
