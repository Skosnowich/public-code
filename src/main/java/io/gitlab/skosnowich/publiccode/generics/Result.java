package io.gitlab.skosnowich.publiccode.generics;

class Result<T> {

	private final boolean success;
	private final T response;

	Result(boolean success, T response) {
		this.success = success;
		this.response = response;
	}

	T getResponse() {
		return response;
	}

	boolean isSuccess() {
		return success;
	}

}
