package io.gitlab.skosnowich.publiccode.generics;

class ServiceA implements Service<ResponseA> {

	@Override
	public Result<ResponseA> serviceMethod() {
		return new Result<>(true, new ResponseA());
	}

}
