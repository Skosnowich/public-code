package io.gitlab.skosnowich.publiccode.generics;

class ServiceB implements Service<ResponseB> {

	@Override
	public Result<ResponseB> serviceMethod() {
		return new Result<>(true, new ResponseB());
	}

}
