package io.gitlab.skosnowich.publiccode.generics;

interface Service<T> {

	Result<T> serviceMethod();

}
